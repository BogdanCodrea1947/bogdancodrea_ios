//
//  Colors.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 1/4/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class Colors {
    class func blue() -> UIColor{
        return UIColor(red: 108.0 / 255.0, green: 153.0 / 255.0, blue: 165.0 / 255.0, alpha: 1);
    }
}