//
//  ImageViewRadius.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 14/02/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation
import QuartzCore

extension UIImageView {
    public func setBorder(size:CGFloat, color:UIColor){
        self.layer.borderWidth = size
        self.layer.borderColor = color.CGColor
        self.layer.cornerRadius = self.frame.size.width / 2
    }
}