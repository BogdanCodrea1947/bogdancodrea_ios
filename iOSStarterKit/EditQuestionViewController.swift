//
//  EditQuestionViewController.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 27/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import UIKit

class EditQuestionViewController: UIViewController {

    @IBOutlet weak var titleEditTextLabel: UITextField!
    @IBOutlet weak var textEditTextLabel: UITextField!
    @IBOutlet weak var saveEditedQuestionButton: UIButton!
    
    var selectedQuestion:Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleEditTextLabel.text = self.selectedQuestion?.title!
        self.textEditTextLabel.text = self.selectedQuestion?.qtext!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveEditedQuestionAction(sender: AnyObject) {
        SVProgressHUD.show()
        self.saveEditedQuestionButton.userInteractionEnabled = false
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        let parameters = ["auth_token": "\(auth_token)","question": ["title": "\(self.titleEditTextLabel.text!)","qtext": "\(self.textEditTextLabel.text!)"]]
        let path = "\(Settings.host())api/questions/\(self.selectedQuestion!.id!)"
        let manager = AFHTTPRequestOperationManager()
        
        manager.PATCH(path, parameters: parameters, success: { (operation, responseObject) in
            if let json = responseObject {
                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()
                self.saveEditedQuestionButton.userInteractionEnabled = true
            }
            
        }) { (operation, error) in
            SVProgressHUD.dismiss()
            let av = UIAlertView(title: "Error!",
                                 message: operation.responseString,
                                 delegate: nil,
                                 cancelButtonTitle: "OK")
            av.show()
            self.saveEditedQuestionButton.userInteractionEnabled = true
        }
        
        
        
    }


}
