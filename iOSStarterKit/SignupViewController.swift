//
//  SignupViewController.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 20/01/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class SignupViewController : UIViewController {
    @IBOutlet var userFirstName            : UITextField!
    @IBOutlet var userLastName             : UITextField!
    @IBOutlet var userEmail                : UITextField!
    @IBOutlet var userPassword             : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonPressed(sender: UIButton){
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func signUpButtonPressed(sender: UIButton) {
        SVProgressHUD.show()
        let manager = AFHTTPRequestOperationManager()
        let parameters = ["user": [
            "first_name": userFirstName.text!,
            "last_name": userLastName.text!,
            "email": userEmail.text!,
            "password": userPassword.text!,
            "password_confirmation": userPassword.text!,
        ]]
        

        manager.POST("\(Settings.host())api/users",
            parameters: parameters,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                print("/api/users success \(responseObject)")
                let sett = Settings.sharedInstance
                sett.authToken = responseObject.objectForKey("authentication_token") as! String
                sett.userId = String(responseObject.objectForKey("id") as! Int)
                sett.currentUser = User(responseObject: responseObject)
                sett.saveInUserDefaults()
                
                SVProgressHUD.dismiss()
                self.performSegueWithIdentifier("home2Segue", sender: nil)
            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                SVProgressHUD.dismiss()
                let av = UIAlertView(title: "Unable to create an account",
                    message: operation.responseString,
                    delegate: nil,
                    cancelButtonTitle: "OK")
                av.show()
            }
        )
    }
}