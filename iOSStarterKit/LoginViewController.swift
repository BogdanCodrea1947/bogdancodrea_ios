//
//  LoginViewController.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 1/4/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation
import UIKit
//import Alamofire

class LoginViewController: UIViewController, UIAlertViewDelegate {
    @IBOutlet var userEmail                : UITextField!
    @IBOutlet var userPassword             : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func logInButtonPressed(sender: UIButton) {
        SVProgressHUD.show()
        let email = userEmail.text
        let password = userPassword.text
        
        let manager = AFHTTPRequestOperationManager()
        manager.POST("\(Settings.host())api/sign_in",
            parameters: ["email": email!,
                         "password": password!],
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                
                let sett = Settings.sharedInstance
                sett.authToken = responseObject.objectForKey("auth_token") as! String
                sett.userId = String(responseObject.objectForKey("user_id") as! Int)
                sett.saveInUserDefaults()
                
                print("successfully signed in")
                self.retrieveCurrentUser()

            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                SVProgressHUD.dismiss()
                let av = UIAlertView(title: "Incorrect credentials",
                    message: "",
                    delegate: nil,
                    cancelButtonTitle: "OK")
                av.show()
            }
        )
        
//        Alamofire.request(.POST, "\(Settings.host())api/sign_in", parameters: ["email" : email!, "password" : password!])
//            .validate()
//            .responseJSON { response in
//                switch response.result {
//                case .Success:
//                    
//                    let sett = Settings.sharedInstance
//                    sett.authToken = response.result.value!.objectForKey("auth_token") as! String
//                    sett.userId = String(response.result.value!.objectForKey("user_id") as! Int)
//                    sett.saveInUserDefaults()
//                    
//                    print("successfully signed in")
//                    self.retrieveCurrentUser()
//
//                case.Failure(let error):
//                    print(error)
//                    SVProgressHUD.dismiss()
//                    let av = UIAlertView(title: "Incorrect credentials",
//                        message: "",
//                        delegate: nil,
//                        cancelButtonTitle: "OK")
//                    av.show()
//                }
//                
//        }
    }
    
    // The user has logged in. Retrieve current user. Then move to appropriate page.
    func retrieveCurrentUser() {
        let sett = Settings.sharedInstance
        let manager = AFHTTPRequestOperationManager()
        
        manager.GET("\(Settings.host())api/users/\(sett.userId)",
            parameters: ["auth_token": sett.authToken],
            success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                
                let us = User(responseObject: responseObject)
                sett.currentUser = us
                
                SVProgressHUD.dismiss()
                self.performSegueWithIdentifier("homeSegue", sender: nil)
                
            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                SVProgressHUD.dismiss()
            }
        )
        
//        Alamofire.request(.GET, "\(Settings.host())api/users/\(sett.userId)", parameters: ["auth_token": sett.authToken])
//            .validate()
//            .responseJSON { response in
//                switch response.result {
//                case .Success:
//                    print("Cool")
//                case.Failure(let error):
//                    print(error)
//                }
//        }


    }
    
    @IBAction func forgotPasswordPressed(sender: UIButton){
        self.view.endEditing(true)
        let alert = UIAlertView(title: "Forgot Your Password?",
                              message: "Enter your email to receive instructions to reset your password.",
                             delegate: self,
                    cancelButtonTitle: "Cancel",
                    otherButtonTitles: "Send")
        
        alert.alertViewStyle = UIAlertViewStyle.PlainTextInput
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex != alertView.cancelButtonIndex){
            let email = alertView.textFieldAtIndex(0)!.text
            
            SVProgressHUD.show()
            let manager = AFHTTPRequestOperationManager()
            manager.POST("\(Settings.host())api/forgot",
                parameters: ["email": email!],
                success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                    SVProgressHUD.dismiss()
                    let av = UIAlertView(title: "Check your e-mail",
                        message: "Please check your e-mail in order to reset your password!",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                    av.show()
                    
                }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                    SVProgressHUD.dismiss()
                    let av = UIAlertView(title: "Not found",
                        message: "We're sorry but your e-mail is not registered to this application!",
                        delegate: nil,
                        cancelButtonTitle: "OK")
                    av.show()
                }
            )
        }
    }
}