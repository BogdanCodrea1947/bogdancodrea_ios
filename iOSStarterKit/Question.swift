//
//  Question.swift
//  iOSStarterKit
//
//  Created by Take Off Labs on 30/05/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import Foundation
class Question:NSObject {
    
    var id:String?
    var user_id:String?
    var qtext:String?
    var title:String?
    var create_time:String?
    var user:String?
    var answers_size:String?
    var canUpdate:String?
    
    init(responseObject: AnyObject!) {
        super.init()
        if let json = responseObject as? Dictionary<String, AnyObject> {
            for field in ["id", "user_id", "qtext", "title", "create_time", "user", "answers_size", "canUpdate"] {
                    if let value = json[field] as? String {
                        self.setValue(value, forKey: field)
                    } else if json[field] is Int {
                        let value = String(json[field] as! Int)
                        self.setValue(value, forKey: field)
                    }
            }
        }
    }
}