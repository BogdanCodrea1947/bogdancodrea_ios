//
//  EditProfileViewController.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 1/22/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation
import QuartzCore


class EditProfileViewController: UIViewController,
                                 UITextViewDelegate,
                                 UINavigationControllerDelegate,
                                 UIImagePickerControllerDelegate,
                                 UIActionSheetDelegate {
    
    @IBOutlet var firstName : UITextField!
    @IBOutlet var lastName : UITextField!
    @IBOutlet var email : UITextField!
    @IBOutlet var password : UITextField!
    @IBOutlet var aboutME : UITextView!
    @IBOutlet var imageView : UIImageView!
    
    @IBOutlet var saveButton : UIButton!
    
    @IBOutlet var aboutView : UIView!
    @IBOutlet var descriptionView: UIView!
    @IBOutlet var tabSegmentedControl: UISegmentedControl!
    
    var aboutViewPosition = 0
    var descriptionViewPosition = 1
    
    var newAvatar = false
    
    // Open up a list of actions from the bottom:
    // From Gallery / From Camera / etc.
    @IBAction func attachImage(sender: UIButton) {
        let destructiveButton:String? = nil
        
        let popup:UIActionSheet = UIActionSheet(title: "Upload an Image",
            delegate: self,
            cancelButtonTitle: "Cancel",
            destructiveButtonTitle: destructiveButton)
        popup.addButtonWithTitle("From Gallery")
        
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            popup.addButtonWithTitle("From Camera")
        }
        
        popup.showInView(UIApplication.sharedApplication().keyWindow!)
        
    }
    
    // One of the Cancel / From Gallery / From Camera buttons have been clicked
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        let title = actionSheet.buttonTitleAtIndex(buttonIndex)
        if (title == "Cancel") {
            // Do nothing
        } else if (title == "From Gallery") {
            self.chooseImage(UIImagePickerControllerSourceType.PhotoLibrary)
        } else if (title == "From Camera") {
            self.chooseImage(UIImagePickerControllerSourceType.Camera)
        }
    }
    
    // Start the image picker, either from gallery or camera
    func chooseImage(sourceType: UIImagePickerControllerSourceType) {
        
        let picker:UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.sourceType = sourceType
        
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        print("Selected Image")        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        
        self.imageView.image = image
        self.newAvatar = true
        self.saveButton.enabled = true
        
        var width = image.size.width
        var height = image.size.height
        let ratio = width / height
        if (width > 300) {
            width = 300
            height = 300 / ratio
        }
        
        if (height > 300) {
            height = 300
            width = height * ratio
        }
        
        image.resize(CGSizeMake(width, height), completionHandler: { [weak self](resizedImage, data) -> () in
            print("Resized image")
            let image = resizedImage
            self?.imageView.image = image
        })
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        print("Cancelled")
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func cancelButtonPressed(sender: UIButton){
        print("Cancel button pressed")
        if let navController = self.navigationController {
            navController.popViewControllerAnimated(true)
        }
        //self.navigationController.popViewControllerAnimated(true)
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(sender: UIButton) {
        print("Save button pressed")
        SVProgressHUD.showWithStatus("Saving Changes")

        let sett = Settings.sharedInstance
        let user = sett.currentUser!

        var user_params = ["first_name": self.firstName.text,
                            "last_name": self.lastName.text,
                            "email": self.email.text,
                         "about_me": self.aboutME.text]

        if (self.password.text as! NSString).length > 0 {
            user_params["password"] = self.password.text
            user_params["password_confirmation"] = self.password.text
        }

        var imageToUpload:NSData?
        if self.newAvatar == true {
            imageToUpload = UIImageJPEGRepresentation(self.imageView.image!, 1.0)
        }

        let serializer = AFHTTPRequestSerializer()
        let request = serializer.multipartFormRequestWithMethod("PATCH", URLString: "\(Settings.host())api/users/\(user.id!)/",
            parameters: ["user": user_params,
                        "auth_token": sett.authToken],
            constructingBodyWithBlock: { (formData: AFMultipartFormData!) in
                if (imageToUpload != nil) {
                    let k:Int = random()
                    
                    formData.appendPartWithFileData(imageToUpload, name: "user[image]", fileName: "avatar\(k).jpg", mimeType: "image/jpeg")
                }
        })

        let manager = AFHTTPRequestOperationManager()
        let operation = manager.HTTPRequestOperationWithRequest(request,
            success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                let newUser = User(responseObject: responseObject)
                sett.currentUser = newUser
                
                SVProgressHUD.dismiss()
                self.navigationController?.popViewControllerAnimated(true)
            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                SVProgressHUD.dismiss()                
                let av = UIAlertView(title: "Error updating account",
                    message: operation.responseString,
                    delegate: nil,
                    cancelButtonTitle: "OK")
                av.show()
            })
        
        operation.start()
        
    }
    
    func textViewDidChange(textView: UITextView) {
        self.saveButton.enabled = true
    }
    
    @IBAction func textFieldDidChange(sender: UITextField) {
        self.saveButton.enabled = true
    }
    
    @IBAction func segmentedControlChanged(sender: UISegmentedControl){
        if self.tabSegmentedControl.selectedSegmentIndex == self.aboutViewPosition{
            self.aboutView.hidden = false
            self.descriptionView.hidden = true
            
        }else if self.tabSegmentedControl.selectedSegmentIndex == self.descriptionViewPosition{
            self.aboutView.hidden = true
            self.descriptionView.hidden = false
        }
    }
    
    override func viewDidLoad() {
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        
        self.firstName.text = user.first_name
        self.lastName.text  = user.last_name
        self.email.text     = user.email
        self.aboutME.text   = user.about_me
        
        if user.image_url != nil {
            self.imageView.setImageWithURL(NSURL(string: user.image_url!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
        
        self.saveButton.enabled = false
        
        super.viewDidLoad()
    }

}