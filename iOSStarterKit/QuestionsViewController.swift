//
//  QuestionsViewController.swift
//  iOSStarterKit
//
//  Created by Take Off Labs on 30/05/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import Foundation
import UIKit

class QuestionsViewController:UIViewController, UITableViewDelegate, UITableViewDataSource, QuestionModelDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var questions:[Question] = [Question]()
    let model:QuestionModel = QuestionModel()
    let refreshController:UIRefreshControl = UIRefreshControl()
    
    var selectedQuestion:Question?
    
    
    func uiRefreshControlAction() {
        model.getQuestions()
        self.tableView.reloadData()
        self.refreshController.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.model.delegate = self
        model.getQuestions()
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.refreshController.addTarget(self, action: "uiRefreshControlAction", forControlEvents: .ValueChanged)
        self.tableView.addSubview(self.refreshController)

        
        
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! QuestionCell
        
        cell.titleLabel.text = self.questions[indexPath.row].title
        
        cell.userLabel.text = self.questions[indexPath.row].user
        
        cell.answersSizeLabel.text = "Answers:" + "\(self.questions[indexPath.row].answers_size!)"
        
        cell.createTileLabel.text = "Asked " + "\(self.questions[indexPath.row].create_time!)" + " ago."
        
        return cell
    }
    func dataReady() {
        self.questions = self.model.questionsArray
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedQuestion = self.questions[indexPath.row]
        self.performSegueWithIdentifier("goToDetail", sender: self)
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToDetail"{
            let answersViewController = segue.destinationViewController as! AnswersViewController
            answersViewController.selectedQuestion = self.selectedQuestion
           
            
        }
    }
    
    
    
    
}