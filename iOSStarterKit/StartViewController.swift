//
//  StartViewController.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 20/01/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class StartViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet var fbLoginView : FBSDKLoginButton!
    
    var fbUserLoggedIn:Bool!
    var counter:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if (FBSDKAccessToken.currentAccessToken() != nil) {
            //user is logged in with fb
            self.fbUserLoggedIn = true
        } else {
        
            self.fbLoginView.delegate = self
            self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends"]
            
            self.fbUserLoggedIn = false
        
        }
        
        let sett = Settings.sharedInstance
        sett.loadFromUserDefaults()
        if (sett.userId != "" &&
            sett.authToken != "" &&
            sett.currentUser == nil) {
            
            self.transitionToHomePage()
        }
        
        
    }
    
    @IBAction func loginButtonPressed(sender: UIButton){
        if self.fbUserLoggedIn == true {
            self.performSegueWithIdentifier("homeSegue", sender: nil)
        } else {
            self.performSegueWithIdentifier("loginSegue", sender: nil)
        }
        
    }
    
    @IBAction func signupButtonPressed(sender: UIButton){
        self.performSegueWithIdentifier("signupSegue", sender: nil)
    }
    
    // Facebook Delegate Methods
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("Facebook User Logged In")
        
        if ((error) != nil)
        {
            print(error)
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
                returnUserData()
            }
        }
    }
    
    func returnUserData()
    {
        let params = ["fields": "email, first_name, last_name"]
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                print(result)
                let firstName : String = result.valueForKey("first_name") as! String
                let lastName : String = result.valueForKey("last_name") as! String
                let userEmail : String = result.valueForKey("email") as! String
                let uid: String = result.valueForKey("id") as! String
                self.authenticateOnServerFromFacebook(firstName, lastName: lastName, userEmail: userEmail, uid: uid)
            }
        })
    }
    
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("Facebook User Logged Out")
        self.fbUserLoggedIn = false
        let FbControl: FBSDKLoginManager = FBSDKLoginManager()
        FbControl.logOut()
    }
    
//    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
//        self.fbUserLoggedIn = true
//        counter += 1
//        if (counter <= 1) {
//            authenticateOnServerFromFacebook(user)
//        }
//    }
    
    func authenticateOnServerFromFacebook(firstName: String, lastName: String, userEmail: String, uid: String) {
        let manager = AFHTTPRequestOperationManager()
        let parameters = ["user": ["uid": uid,
            "first_name": firstName,
            "last_name": lastName,
            "email": userEmail]]
        
        SVProgressHUD.show()
        manager.POST("\(Settings.host())api/sessions/facebook",
            parameters: parameters,
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                
                let sett = Settings.sharedInstance
                sett.authToken = responseObject.objectForKey("auth_token") as! String
                sett.userId = String(responseObject.objectForKey("user_id") as! Int)
                sett.saveInUserDefaults()
                
                self.transitionToHomePage()
            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                SVProgressHUD.dismiss()
                let av = UIAlertView(title: "Unable to log you in",
                    message: operation.responseString,
                    delegate: nil,
                    cancelButtonTitle: "OK")
                av.show()
            }
        )
    }
    
    func transitionToHomePage() {
        let manager = AFHTTPRequestOperationManager()
        let sett = Settings.sharedInstance
        
        manager.GET("\(Settings.host())api/users/\(sett.userId)",
            parameters: ["auth_token": sett.authToken],
            success: { (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                
                let us = User(responseObject: responseObject)
                sett.currentUser = us
                
                SVProgressHUD.dismiss()
                self.performSegueWithIdentifier("homeSegue", sender: nil)
                
            }, failure: { (operation: AFHTTPRequestOperation!, error: NSError!) in
                
            }
        )
    }
    
    
//    func loginView(loginView : FBLoginView!, handleError:NSError) {
//        print("Error: \(handleError.localizedDescription)")
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}