//
//  User.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 1/22/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class User: NSObject {
    var id: String?
    var email: String?
    
    var authentication_token: String?
    var provider: String?
    var uid: String?
    
    var first_name: String?
    var last_name: String?
    
    var role: String?
    var about_me: String?
    var company_id: String?
    
    var image_url: String?;
    
    // Initialize a User object from a JSON response
    init(responseObject: AnyObject!) {
        super.init()
        if let json = responseObject as? Dictionary<String, AnyObject> {
            for field in ["id", "email", "authentication_token", "provider",
                          "first_name", "last_name", "about_me", "image_url"] {
                if let value = json[field] as? String {
                    self.setValue(value, forKey: field)
                } else if json[field] is Int {
                    let value = String(json[field] as! Int)
                    self.setValue(value, forKey: field)
                }
            }
        }
    }
    
    // Return full name of user
    func fullName() -> String {
        var result = ""
        if (self.first_name != nil) {
            result = result + (self.first_name!) + " "
        }
        
        if (self.last_name != nil) {
            result = result + (self.last_name!)
        }
        
        return result
    }
    
}