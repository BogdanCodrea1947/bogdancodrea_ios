//
//  AnswerModel.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 08/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import UIKit
import Foundation

protocol AnswerModelDelegate{
    func dataReady()
    
}

class AnswerModel: NSObject {
    var answersArray = [Answer]()
    var delegate:AnswerModelDelegate?
    
    func getAnswers(question_id: Int){
        
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        var answers = [Answer]()
        let parameters = [auth_token]
        SVProgressHUD.show()
        let manager = AFHTTPRequestOperationManager()
        
        
        manager.GET("\(Settings.host())api/questions/\(question_id)/answers" , parameters: parameters , success: { (operation, responseObject) in
            if let jsonArray = responseObject as? NSArray{
                for elem in jsonArray {
                    let answer = Answer.init(responseObject: elem)
                    answers.append(answer)
                    
                }
            }
            else {
                print("Not an array")
            }
            self.answersArray = answers
            
            if self.delegate != nil{
                self.delegate!.dataReady()
            }
            SVProgressHUD.dismiss()
            
        }) { (operation, error) in
            SVProgressHUD.dismiss()
            let av = UIAlertView(title: "Unable to load all answers",
                                 message: operation.responseString,
                                 delegate: nil,
                                 cancelButtonTitle: "OK")
            av.show()
        }
    }


}
