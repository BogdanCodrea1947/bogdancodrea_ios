//
//  File.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 14/02/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class ApplicantCell : UITableViewCell {
    
    @IBOutlet var avatarImage : UIImageView!
    @IBOutlet var applicantNameLabel : UILabel!
    @IBOutlet var applicantHistoryLabel : UILabel!
    @IBOutlet var applicantSkillsLabel : UILabel!
}
