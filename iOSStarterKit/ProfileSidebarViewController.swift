//
//  JobseekerSidebarViewController.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 2/14/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class ProfileSidebarViewController: UITableViewController {
    let menuItems = ["profile", "logout", "questions"]
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuItems.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if ("logout" == self.menuItems[indexPath.row]){
            let FbControl: FBSDKLoginManager = FBSDKLoginManager()
            FbControl.logOut()
            let sett = Settings.sharedInstance
            sett.logout()
            self.performSegueWithIdentifier("logOutSegue", sender: self)
        }
//     
//        if ("profile" == self.menuItems[indexPath.row]) {
//            self.performSegueWithIdentifier("profileSegue", sender: self)
//        }
    }
}