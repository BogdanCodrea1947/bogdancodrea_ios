//
//  NewAnswerViewController.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 28/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import UIKit

class NewAnswerViewController: UIViewController {

    @IBOutlet weak var answerTextField: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var selectedQuestion:Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveButtonAction(sender: AnyObject) {
        SVProgressHUD.show()
        self.saveButton.userInteractionEnabled = false
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        let parameters = ["auth_token": "\(auth_token)", "answer": ["body": "\(self.answerTextField.text!)"]]
        let path = "\(Settings.host())api/questions/\(self.selectedQuestion!.id!)/answers"
        let manager = AFHTTPRequestOperationManager()
        manager.POST(path, parameters: parameters, success: { (operation, responseObject) in
            if let json = responseObject {
                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()
                self.saveButton.userInteractionEnabled = true
            }
            
        }) { (operation, error) in
            SVProgressHUD.dismiss()
            let av = UIAlertView(title: "Error!!!",
                                 message: operation.responseString,
                                 delegate: nil,
                                 cancelButtonTitle: "OK")
            av.show()
            self.saveButton.userInteractionEnabled = true
        }
        
        
    }

    
}
