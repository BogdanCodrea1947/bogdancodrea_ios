//
//  Settings.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 1/20/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

private let _SettingsSharedInstance = Settings()

class Settings {
    var userId: String = ""
    var authToken: String = ""
    
    var currentUser: User? = nil
    
    class func host() -> String {
        #if arch(i386) || arch(x86_64)
            //simulator
            return "http://localhost:3000/"
        #else
            //device
            return "http://yourapp.herokuapp.com/"
        #endif
    }
    
    class var sharedInstance: Settings {
        return _SettingsSharedInstance
    }
    
    // Log out current user
    func logout() {
        self.currentUser = nil
        self.userId = ""
        self.authToken = ""
        self.saveInUserDefaults()
    }
    
    // Load user from user defaults
    func loadFromUserDefaults() {
        print("Loading from user defaults")
        let defaults = NSUserDefaults.standardUserDefaults()
        if let user_id = defaults.stringForKey("userId") {
            self.userId = user_id
        }
        
        if let auth_token = defaults.stringForKey("authToken") {
            self.authToken = auth_token
        }
    }
 
    // Save user in user defaults
    func saveInUserDefaults() {
        NSLog("Saving in user defaults")
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.userId, forKey: "userId")
        defaults.setObject(self.authToken, forKey: "authToken")
    }
}