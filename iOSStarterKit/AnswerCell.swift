//
//  AnswerCell.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 08/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//
import Foundation
import UIKit

class AnswerCell: UITableViewCell {

    @IBOutlet weak var emailAnswerLabel: UILabel!
    
    @IBOutlet weak var bodyAnswerLabel: UILabel!
    
    @IBOutlet weak var likesAnswerLabel: UILabel!
    
    @IBOutlet weak var dislikesAnswerLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
}
