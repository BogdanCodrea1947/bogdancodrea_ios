//
//  Answer.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 08/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import UIKit
import Foundation

class Answer: NSObject {
    
    var id:String?
    var body:String?
    var question_id:String?
    var user_id:String?
    var like_ids:String?
    var number_of_likes:String?
    var dislike_ids: String?
    var number_of_dislikes:String?
    var email:String?
    
    init(responseObject: AnyObject!) {
        super.init()
        if let json = responseObject as? Dictionary<String, AnyObject> {
            for field in ["id", "body", "question_id", "user_id", "like_ids", "number_of_likes", "dislike_ids", "number_of_dislikes", "email"] {
                if let value = json[field] as? String {
                    self.setValue(value, forKey: field)
                } else if json[field] is Int {
                    let value = String(json[field] as! Int)
                    self.setValue(value, forKey: field)
                }
            }
        }
    }

}
