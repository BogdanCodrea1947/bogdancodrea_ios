//
//  JobseekerBaseViewController.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 2/14/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class ProfileBaseViewController: UIViewController {
    
    func initSidebarButton(sidebarButton: UIButton){
        // Navigation bar button click
        sidebarButton.addTarget(self.revealViewController(), action: Selector("revealToggle:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        // Pan gesture on screen
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
}

class ApplicationTableViewController: UITableViewController {
    func initSidebarButton(sidebarButton: UIButton){
        // Navigation bar button click
        sidebarButton.addTarget(self.revealViewController(), action: Selector("revealToggle:"), forControlEvents: UIControlEvents.TouchUpInside)
        
        // Pan gesture on screen
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
}