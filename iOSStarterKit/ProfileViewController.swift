//
//  EmployerProfileViewController.swift
//  iOS Labs
//
//  Created by Alexandru Tandrau on 1/22/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation
import QuartzCore

class ProfileViewController: ProfileBaseViewController {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var bio: UITextView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var sidebarButton: UIButton!
    
    override func viewDidLoad() {
        initSidebarButton(sidebarButton)
    }
    
    override func viewWillAppear(animated: Bool) {
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        
        self.name.text = user.fullName()
        self.bio.text = user.about_me
        
        if user.image_url != nil {
            self.imageView.setImageWithURL(NSURL(string: user.image_url!), placeholderImage: UIImage(named: "placeholder.png"))
        }
        
        self.imageView.setBorder(4.0, color: UIColor.whiteColor())
        
        super.viewWillAppear(animated)
    }
    
    @IBAction func clickedLogOut(sender: UIButton) {
        let FbControl: FBSDKLoginManager = FBSDKLoginManager()
        FbControl.logOut()
        let sett = Settings.sharedInstance
        sett.logout()
        self.performSegueWithIdentifier("logOutSegue", sender: self)
    }
    
}