//
//  QuestionCell.swift
//  iOSStarterKit
//
//  Created by Take Off Labs on 30/05/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import Foundation
import UIKit

class QuestionCell:UITableViewCell {
    

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var answersSizeLabel: UILabel!
    
    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var createTileLabel: UILabel!
   
}