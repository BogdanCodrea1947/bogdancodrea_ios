import Foundation

protocol QuestionModelDelegate {
    func dataReady()
}
class QuestionModel:NSObject{

    var questionsArray = [Question]()
    var delegate:QuestionModelDelegate?
    
    func getQuestions(){
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        var questions = [Question]()
        let parameters = [auth_token]
        SVProgressHUD.show()
        let manager = AFHTTPRequestOperationManager()
        
        
        
        manager.GET("\(Settings.host())api/questions", parameters: parameters , success: { (operation, responseObject) in
            if let jsonArray = responseObject as? NSArray{
                for elem in jsonArray {
                    let question = Question.init(responseObject: elem)
                    questions.append(question)
                    
                }
            }
            else {
                print("Not an array")
            }
            self.questionsArray = questions
            
            if self.delegate != nil{
                self.delegate!.dataReady()
            }
            SVProgressHUD.dismiss()
            
            }) { (operation, error) in
                SVProgressHUD.dismiss()
                let av = UIAlertView(title: "Unable to load all questions",
                                     message: operation.responseString,
                                     delegate: nil,
                                     cancelButtonTitle: "OK")
                av.show()
        }
    }


}