//
//  CustomButton.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 1/4/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation
import UIKit

class CustomButton: UIButton{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 4.0
        if(self.state == UIControlState.Disabled){
            self.layer.borderColor = UIColor.grayColor().CGColor
        } else {
            self.layer.borderColor = Colors.blue().CGColor
        }
        
        self.layer.borderWidth = 1.5
        
        self.tintColor = Colors.blue()
    }
}