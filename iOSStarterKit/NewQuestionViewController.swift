//
//  NewQuestionViewController.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 09/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import UIKit
import Foundation

class NewQuestionViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var bodyTextField: UITextField!
    @IBOutlet weak var saveQuestionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    @IBAction func saveQuestion(sender: AnyObject) {
        SVProgressHUD.show()
        self.saveQuestionButton.userInteractionEnabled = false
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        let parameters = ["auth_token": "\(auth_token)","question": ["title": "\(self.titleTextField.text!)","qtext": "\(self.bodyTextField.text!)"]]
        let path = "\(Settings.host())api/questions/"
        let manager = AFHTTPRequestOperationManager()
        manager.POST(path, parameters: parameters, success: { (operation, responseObject) in
            if let json = responseObject {

                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()
                self.saveQuestionButton.userInteractionEnabled = true
            }
            
            }) { (operation, error) in
                SVProgressHUD.dismiss()
                let av = UIAlertView(title: "Error!",
                                     message: operation.responseString,
                                     delegate: nil,
                                     cancelButtonTitle: "OK")
                av.show()
                self.saveQuestionButton.userInteractionEnabled = true
        }
        
                
        
    }

  
}
