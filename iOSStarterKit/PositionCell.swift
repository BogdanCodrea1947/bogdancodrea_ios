//
//  PositionCell.swift
//  iOS Labs
//
//  Created by Flavia Grosan on 14/02/15.
//  Copyright (c) 2015 iOS Labs. All rights reserved.
//

import Foundation

class PositionCell : UITableViewCell {
    
    @IBOutlet var companyLogoImage : UIImageView!
    @IBOutlet var positionNameLabel : UILabel!
    @IBOutlet var companyNameLabel : UILabel!
    @IBOutlet var distanceLabel : UILabel!
}