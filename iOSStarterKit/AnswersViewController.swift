//
//  AnswersViewController.swift
//  iOSStarterKit
//
//  Created by Codrea Bogdan on 08/06/16.
//  Copyright © 2016 Blaze Jobs. All rights reserved.
//

import Foundation
import UIKit

class AnswersViewController:UIViewController, UITableViewDelegate, UITableViewDataSource, AnswerModelDelegate {

    @IBOutlet weak var scrollViewController: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var questionBodyLabel: UILabel!
    
    @IBOutlet weak var editQuestionButton: UIBarButtonItem!
    
    @IBOutlet weak var deleteQuestionButton: UIBarButtonItem!
       
    var selectedQuestion:Question?
    var currentUserId: String?
    
    var answers:[Answer] = [Answer]()
    let model:AnswerModel = AnswerModel()
    let refreshController:UIRefreshControl = UIRefreshControl()
    var selectedAnswer:Answer?
    
    func uiRefreshControlAction() {
        model.getAnswers(Int((self.selectedQuestion?.id)!)!)
        self.tableView.reloadData()
        self.refreshController.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        self.currentUserId = user.id!
        
        if selectedQuestion?.user_id! != user.id!{
            self.editQuestionButton.title = ""
            self.editQuestionButton.enabled = false
            self.deleteQuestionButton.title = ""
            self.deleteQuestionButton.enabled = false
        }
        
               
        self.scrollViewController.contentSize = CGSizeMake(500, 1000)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.model.delegate = self
        
        model.getAnswers(Int((self.selectedQuestion?.id)!)!)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.refreshController.addTarget(self, action: "uiRefreshControlAction", forControlEvents: .ValueChanged)
        self.tableView.addSubview(self.refreshController)
        self.scrollViewController.addSubview(self.refreshController)
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if let ques = self.selectedQuestion {
            self.questionTitleLabel.text = ques.title
            self.questionBodyLabel.text = ques.qtext
        }
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.answers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("answerCell") as! AnswerCell
        self.selectedAnswer = answers[indexPath.row]
        
        if self.answers[indexPath.row].user_id! == self.currentUserId {
            
            cell.deleteButton.hidden = false
            
        }
        else {
            cell.deleteButton.hidden = true
        }
        
        cell.deleteButton.tag = indexPath.row
        
        cell.emailAnswerLabel.text = "By: " + "\(self.answers[indexPath.row].email!)"
        cell.bodyAnswerLabel.text = self.answers[indexPath.row].body
        cell.likesAnswerLabel.text = "Likes: " + "\(self.answers[indexPath.row].number_of_likes!)"
        cell.dislikesAnswerLabel.text = "Dislikes: " + "\(self.answers[indexPath.row].number_of_dislikes!)"
        return cell
        
   }
    func dataReady() {
        self.answers = self.model.answersArray
        tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToEdit"{
            let editQuestionViewController = segue.destinationViewController as! EditQuestionViewController
            editQuestionViewController.selectedQuestion = self.selectedQuestion
            
        }
        if segue.identifier == "goToNewAnswer"{
            let newAnswerViewController = segue.destinationViewController as! NewAnswerViewController
            newAnswerViewController.selectedQuestion = self.selectedQuestion
        }
    }
    
    @IBAction func deleteQuestionButtonAction(sender: AnyObject) {
        SVProgressHUD.show()
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        let parameters = ["auth_token": "\(auth_token)","question": ["id": "\(self.selectedQuestion!.id!)"]]
        let path = "\(Settings.host())api/questions/\(self.selectedQuestion!.id!)"
        let manager = AFHTTPRequestOperationManager()
        manager.DELETE(path, parameters: parameters, success: { (operation, responseObject) in
            if let json = responseObject {
                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()
                
            }
            
        }) { (operation, error) in
            SVProgressHUD.dismiss()
            let av = UIAlertView(title: "Error!",
                                 message: operation.responseString,
                                 delegate: nil,
                                 cancelButtonTitle: "OK")
            av.show()
          
        }
        
        
        
    }
    
    @IBAction func deleteAnswerAction(sender: AnyObject) {
        SVProgressHUD.show()
        let sett = Settings.sharedInstance
        let user = sett.currentUser!
        let auth_token = user.authentication_token!
        let parameters = ["auth_token": "\(auth_token)","answer": ["id": "\(self.selectedAnswer!.user_id!)"]]
        let path = "\(Settings.host())api/questions/\(self.selectedQuestion!.id!)/answers/\(self.selectedAnswer!.id!)"
        let manager = AFHTTPRequestOperationManager()
        manager.DELETE(path, parameters: parameters, success: { (operation, responseObject) in
            if let json = responseObject {
                self.navigationController?.popViewControllerAnimated(true)
                SVProgressHUD.dismiss()
                
            }
            
        }) { (operation, error) in
            SVProgressHUD.dismiss()
            let av = UIAlertView(title: "Error!",
                                 message: operation.responseString,
                                 delegate: nil,
                                 cancelButtonTitle: "OK")
            av.show()
            
        }
        
    }

}
